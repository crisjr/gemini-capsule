(ns br.eng.crisjr.gemini.gemlog.setup
  (:require [babashka.curl :as curl]
            [cheshire.core :as json]
            [clojure.string :as string]))

(def github-repo "ishiikurisu/notes")
(def branch "master")
(def repo-url (str "https://raw.githubusercontent.com/" github-repo "/" branch "/"))
(def index-url (str repo-url "index.blog.json"))
(def index-template-file "./index.template.gmi")
(def post-template-file "./post.template.gmi")

(defn- clean-index-string [s]
  (->> s
       (string/trim)
       (filter #(not= (int %) 0xfeff))
       (apply str)))

(defn- load-index [url]
  (-> url
      (curl/get)
      (get :body)
      (clean-index-string)
      (json/parse-string)))

(defn- select-posts [index]
  (filter #(.contains (get % "path") ".gmi")
          index))

(defn- build-link-text-date [entry]
  (-> (get entry "date")
      (string/split #"T")
      (first)))

(defn- build-link-text [entry]
  (let [title (get entry "title")
        date (build-link-text-date entry)]
    (str date " " title)))

(defn- index-to-links [index]
  (->> index
       (map (fn [entry] (let [href (str "./" (get entry "path"))
                              text (build-link-text entry)]
                          (str "=> " href " " text))))
       (string/join "\n")))

(defn- save-index [index]
  (let [template (slurp index-template-file)
        links (index-to-links index)
        filled (string/replace template "{{links}}" links)]
    (spit "index.gmi" filled)))

(defn- fill-post-template [template post]
  (string/replace template "{{post}}" post))

(defn- download-posts [index]
  (mapv #(let [post-template (slurp post-template-file)
               path (get % "path")
               url (str repo-url path)]
           (->> url
                (slurp)
                (fill-post-template post-template)
                (spit path))) 
        index))

(defn -main []
  (let [index (-> index-url (load-index) (select-posts))]
    (save-index index)
    (download-posts index)
    (println "done!")))

(-main)

