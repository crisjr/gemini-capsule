# Minha cápsula pessoal

Acessível pelo protocol Gemini.

Este projeto é possível graças aos seguintes projetos _open source_:

- [Gemini](https://gemini.circumlunar.space/)
- [Space Age](https://gitlab.com/lambdatronic/space-age)
